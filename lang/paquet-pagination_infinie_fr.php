<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'pagination_infinie_description' => '',
	'pagination_infinie_nom' => 'Pagination infinie',
	'pagination_infinie_slogan' => 'Une pagination qui n\'en finit pas',
);
