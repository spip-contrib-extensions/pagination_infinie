/* Pagination infinie manuelle */
function pagination_infinie_loading(url,href,options) {jQuery(this).find('.pagination-items.pagination_infinie').addClass('loading');}
function pagination_infinie_loaded(c, href, history) {
	var $me = jQuery(this);
	$me.find('.pagination-items.pagination_infinie').closest('.pagination').remove();
	if ($me.find('.pagination-infinie-contenu').length){
		var content = jQuery('<div>'+c+'</div>');
		content = content.find('.pagination-infinie-contenu').html();
		$me.find('.pagination-infinie-contenu').append(content);
	} else {
		var content = jQuery(c);
		$me.append(content);
	}
}
jQuery(function() {
	jQuery('.pagination-items.pagination_infinie')
		.closest('div.ajaxbloc')
		.attr('data-loading-callback','pagination_infinie_loading')
		.attr('data-loaded-callback','pagination_infinie_loaded');
});