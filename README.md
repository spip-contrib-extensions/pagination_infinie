# Pagination infinie


![pagination_infinie](./prive/themes/spip/images/pagination_infinie-xx.svg)

Ce plugin propose un modèle de pagination infinie utilisable simplement :

```
<BOUCLE_articles(ARTICLES) {!par date} {pagination}>
    <div class="hentry clearfix">
        [(#LOGO_ARTICLE_RUBRIQUE{#URL_ARTICLE}|image_reduire{150,*})]
        <h3 class="h2 entry-title"><a href="#URL_ARTICLE" rel="bookmark">#TITRE</a></h3>
        <small><abbr class="published">[(#DATE|affdate_jourcourt)]</abbr>[, <:par_auteur:> (#LESAUTEURS|supprimer_tags)]</small>
        [<div class="#EDIT{intro} introduction entry-content">(#INTRODUCTION)</div>]
    </div>
</BOUCLE_articles>
[<nav role="navigation" class="p pagination">(#PAGINATION{infinie})</nav>]
</B_articles>
```

Par défaut, le bouton affiche un label "Plus…" correspondant à la chaine de langue `pagination_infinie:label_plus`

Il est possible de le personaliser au cas par cas via

```
[<nav role="navigation" class="p pagination">(#PAGINATION{infinie,label_plus="Voir plus d'articles…"})</nav>]
```

## Fonctionnement

Le modèle `modeles/pagination_infinie.html` insère également le script `javascript/pagination_infinie.js` qui se charge d'ajouter une callback en debut et en fin de loading de la pagination.
La callback de début de loading `pagination_infinie_loading` pose une class `.loading` sur la pagination, ce qui permet d'afficher un loader svg pendant le chargement des contenus.
Après chargement des contenu, la callback `pagination_infinie_loaded` prends la main sur le comportement normal de la pagination et ajoute les nouveaux contenus à ceux déjà affichés.


## Cas des boucles avec parties avant/après

La pagination recharge tout le contenu de la boucle, ce qui inclue 
* l'entête de boucle `<BB_xx>`
* la partie conditionnelle avant ``<B_xx>``
* la partie conditionnelle après ``</B_xx>``
* le pieds de boucle `<BB_xx>`

Si votre boucle utilise ces parties là, alors le html qu'elles produisent sera envoyé et affiché à chaque fois qu'une nouvelle page est chargée, 
ce qui va être totalement redondant.

Par exemple dans le cas ci-dessous, le titre `<h2>Articles récents</h2>` sera réaffiché à chaque clic sur "Plus...", et surtout on réinjectera à chaque fois le conteneur
`<div class="menu menu_articles" id="articles_recents">`

```
<B_articles>
<div class="menu menu_articles" id="articles_recents">
	<h2>Articles récents</h2>
	#ANCRE_PAGINATION
	<ul>
		<BOUCLE_articles(ARTICLES) {!par date} {pagination}>
		<li dir="#LANG_DIR" class="hentry clearfix text-#LANG_LEFT">
			[(#LOGO_ARTICLE_RUBRIQUE{#URL_ARTICLE}|image_reduire{150,*})]
			<h3 class="h2 entry-title"><a href="#URL_ARTICLE" rel="bookmark">#TITRE</a></h3>
			<small><abbr class="published"[ title="(#DATE|date_iso)"]>[(#DATE|affdate_jourcourt)]</abbr>[, <:par_auteur:> (#LESAUTEURS|supprimer_tags)]</small>
			[<div class="#EDIT{intro} introduction entry-content">(#INTRODUCTION)</div>]
		</li>
		</BOUCLE_articles>
	</ul>
	[<nav role="navigation" class="p pagination">(#PAGINATION{infinie,label_plus="Voir plus d'articles…"})</nav>]
</div><!--#articles_recents-->
</B_articles>
```

Pour remédier à cela, il est possible d'indiquer à la pagination quel est le contenu à réinejcter, via une classe `.pagination-infinie-contenu` comme suit :

```
<B_articles>
<div class="menu menu_articles" id="articles_recents">
	<h2>Articles récents</h2>
	#ANCRE_PAGINATION
    <div class="pagination-infinie-contenu">
	<ul>
		<BOUCLE_articles(ARTICLES) {!par date} {pagination}>
		<li dir="#LANG_DIR" class="hentry clearfix text-#LANG_LEFT">
			[(#LOGO_ARTICLE_RUBRIQUE{#URL_ARTICLE}|image_reduire{150,*})]
			<h3 class="h2 entry-title"><a href="#URL_ARTICLE" rel="bookmark">#TITRE</a></h3>
			<small><abbr class="published"[ title="(#DATE|date_iso)"]>[(#DATE|affdate_jourcourt)]</abbr>[, <:par_auteur:> (#LESAUTEURS|supprimer_tags)]</small>
			[<div class="#EDIT{intro} introduction entry-content">(#INTRODUCTION)</div>]
		</li>
		</BOUCLE_articles>
	</ul>
	[<nav role="navigation" class="p pagination">(#PAGINATION{infinie,label_plus="Voir plus d'articles…"})</nav>]
	</div>
</div><!--#articles_recents-->
</B_articles>
```

Dans ce cas, le script sait que seul l'intérieur de ce `<div class="pagination-infinie-contenu"> ... </div>` 
doit être pris en compte, et il sera ajouté à celui déjà présent.

Attention a bien y inclure la `#PAGINATION` pour avoir un lien vers la page suivante !
Par exemple ici on ne peut pas mettre la classe sur le `<ul>` car sinon la nouvelle page chargée n'aurait pas de pagination.
Du coup cela veut dire aussi qu'après chaque chargement on aura un `<ul>...</ul>` de plus, et non les `<li>..</li>` qui s'ajoutent à la liste précédente.
